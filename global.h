//全局变量和函数
#pragma once
#include "graphics.h"
#include <iostream>
#include <assert.h>
#include <vector>
#define EPS 1e-5 //浮点数误差判断
#define MAX_LEVEL_RANGE 500 //最大关卡长度
#define MAX_LEVEL_LAYER 6 //最大显示图层

extern int SCORE; //分数
extern int COLLIDER_ID; //角色ID(自增值)
extern int COIN_TOTAL; ///金币数量
extern int LIVES; //剩余生命
extern int TOP_SCORE; //最高分数
extern int START_TIME;
extern int NOW_SCENR;
extern int PLAYERS_NUM;
extern bool DEBUG_MODE; //是否处于调试模式
extern double GRAVITY; //重力值
extern std::string LEVEL_NAME; //当前关卡名字

struct Costume {
	int a, b, c;
	bool operator == (const Costume& p);
};

extern PIMAGE getZoomImageCopy(PIMAGE pimg, int zoomWidth, int zoomHeight);
extern void zoomImage(PIMAGE& pimg, int zoomWidth, int zoomHeight);
extern void zoomImage(PIMAGE& pimg, float scale);
extern void getimage(PIMAGE pDstImg, LPCSTR pImgFile, int srcX, int srcY, int srcWidth, int srcHeigh);
extern void getimage1(PIMAGE pDstImg, LPCSTR pImgFile, int srcX, int srcY, int dstX, int dstY);
extern void mirror_image(PIMAGE& pimg);
extern void copyimage(PIMAGE& pDstimg, PIMAGE psrcimg);
extern PIMAGE getZoomImageCopy(PIMAGE pimg, int zoomWidth, int zoomHeight);